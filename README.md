<h1 align="center">WinClassic (ALPHA)</h1>
<p align="center">Windows Classic theme for Discord - It sucks less.</p>

# ![screenshot](https://imgur.com/RjsgItd.png)

## Things to note: 
* This is a fork
* This theme is in alpha stages - **things will be broken**
* The CSS is spagethi and needs to be organized
* You should really only use this if you want to give feedback, contribute, or you're just that deprived


## How to install:

* Learn how to use [BeautifulDiscord](https://github.com/leovoel/BeautifulDiscord), [Powercord](https://github.com/powercord-org/powercord), [Goosemod](https://goosemod.com/), or [BetterDiscord](https://github.com/rauenzi/BetterDiscordApp).

* Download [WinClassic.user.css](https://raw.githubusercontent.com/SlippingGitty/WinClassic/master/WinClassic.theme.css) and either
  * inject the CSS file with BeautifulDiscord
  * place in your BetterDiscord themes folder
* Powercord and Vizality users
  * open a terminal in your themes folder and type `git clone https://github.com/SlippingGitty/WinClassic`
* Goosemod users
  * Download [WinClassicTheme.js](https://raw.githubusercontent.com/SlippingGitty/WinClassic/master/WinClassicTheme.js) and import the file from the "Local Modules" tab.

## Devs, Contributors, and other Creditors:

| <a href="https://github.com/SlippingGitty" target="_blank"> <img src="https://avatars.githubusercontent.com/u/76500838?s=460&u=109f1c2012f3e452251391807262ed098f45ec94&v=4" alt="" width="96px" height="96px"> </a> | <a href="https://github.com/Euphorianic" target="_blank"> <img src="https://avatars.githubusercontent.com/u/73035923?s=460&u=1c5fb61c01699a43288a53ff453b70254ba714b5&v=4" alt="" width="96px" height="96px"> </a> |
|:-:|:-:|
| SlippingGitty | Pavol/Euphorianic |
